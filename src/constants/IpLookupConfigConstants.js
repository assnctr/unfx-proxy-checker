export const LOOKUP_URL = 'https://api.openproxy.space/ip';

export const LOOKUP_CONFIG = {
    timeout: 5000,
    headers: {
        'User-Agent': 'UNFX IP LOOKUP'
    }
};
